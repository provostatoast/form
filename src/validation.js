const nameValidation = (value) => {
    if (!value) {
        return 'Name field cannot be blank.';
    }
    if (!/^[a-zA-Z\s]*$/.test(value)) {
        return 'Only letters and spaces allowed.';
    }
    return;
}

const cardValidation = (value) => {
    if (!value) {
        return 'Card field cannot be blank.';
    }
    /**
     *  CARD LENGTH VALIDATION
     */
    // AMEX
    if (/^3[47]/.test(value) && value.length !== 15) {
        return 'Amex cards must have 15 characters.'
    }
    // VISA
    if (/^4/.test(value) && value.length !== 16) {
        return 'Visa cards must have 16 characters.'
    }
    return;
}

const activeCardCheck = (value) => {
    // ensure the value consists only of numbers\
    if (!/^\d+$/.test(value)) {
        return {
            type: '',
            error: 'Only numbers allowed.'
        }
    }
    // Ensure the value begins with a 4, 34, or 37
    if (
        !/^4|^3[47]/.test(value)
    ) {
        return {
            type: '',
            error: 'Please use a Visa or Amex card.'
        }
    }
    // It's a visa
    if (/^4/.test(value)) {
        return {
            type: 'visa',
            error: ''
        }
    }
    // It's an AMEX
    if (/^3[47]/.test(value)) {
        return {
            type: 'amex',
            error: ''
        }
    }   
}

const cvvValidation = (value, type) => {
    // ensure a value
    if (!value) {
        return 'Please enter the 3 or 4 digit security code.';
    }
    // ensure the value consists only of numbers\
    if (!/^\d+$/.test(value)) {
        return {
            type: '',
            error: 'Only numbers allowed.'
        }
    }
    // ensures a valid card type exists
    if (!type) {
        return 'Please enter a valid CVV number.'
    }
    // makes sure amex cards have only 4 characters
    if (type === 'amex' && value.length !== 4) {
        return 'Please enter the 4 digit security code on the front of the card.'
    }
    // makes sure visa cards have only 3 characters
    if (type === 'visa' && value.length !== 3) {
        return 'Please enter the 3 digit security code on the back of the card.'
    }
}

const monthValidation = (value) => {
    // ensure a value
    if (!value) {
        return 'Please enter the 2 digit month.';
    }
    // ensure the value consists only of numbers\
    if (!/^\d+$/.test(value)) {
        return 'Only numbers allowed.'
    }
    if (+value < 1 || +value > 12) {
        return 'Please enter a value between 01 and 12.'
    }
}

const yearValidation = (value) => {
    // ensure a value
    if (!value || value.lenth < 4) {
        return 'Please enter the 4 digit year.';
    }
    // ensure the value consists only of numbers\
    if (!/^\d+$/.test(value)) {
        return 'Only numbers allowed.'
    }
    // makes sure amex cards have only 4 characters
    if (+value < 2021 ) {
        return 'Please enter a valid year.'
    }
}

export {
    nameValidation,
    cardValidation,
    activeCardCheck,
    cvvValidation,
    monthValidation,
    yearValidation,
}