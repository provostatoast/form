import React, {useContext, useState} from 'react';
import FormContext from '../FormContext';
import { cardValidation } from '../../../validation';
import { formatCardField } from '../../../helpers/cardFormatter';

const CardInput = ({
        name,
        placeholder,
        type = 'text',
        setCardType = '',
        cardType = '',
        activeCheck = () => {},
        validation = () => {},
        ...props}) => {
    const {setValue, setError, values, errors} = useContext(FormContext);
    const [fieldValue, setFieldValue] = useState(values[name]);
    return (
        <div className="field">
            <input 
                placeholder={placeholder}
                name={name}
                type={type}
                value={fieldValue}
                onChange={({target}) => {
                    const {value} = target;
                    const {numsOnly, formattedValue} = formatCardField(value, cardType);
                    const {type: t, error} = activeCheck(numsOnly);
                    if (error) {
                        setError(name, error);
                        setCardType('')
                    } else {
                        setError(name, '');
                        setCardType(t);
                    }
                    setFieldValue(formattedValue);
                    setValue(name, numsOnly);
                }}
                onBlur={() => {
                    const {numsOnly} = formatCardField(fieldValue);
                    const error = cardValidation(numsOnly);
                    setError(name, error);
                }}
                {...props}
            />
            {errors[name] && (
                <div className="error">{errors[name]}</div>
            )}
        </div>
    )
}

export default CardInput;