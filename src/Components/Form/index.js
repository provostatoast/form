import React, {useState} from 'react';
import FormContext from './FormContext';

const Form = ({initialState = {cardNumber: ''}, children}) => {

    const [state, setState] = useState({
      values: initialState,
      cardType: '',
      errors: {},
    })
    const setValue = (name, value) => {
      const {values} = state;
      values[name] = value;
      setState({
        ...state,
        values,
      })
    }
    const setError = (name, error) => {
      const {errors} = state;
      errors[name] = error;
      setState({
        ...state,
        errors,
      })
    }

    return (
      <div className="formContainer">
        <h3>Enter your credit card information</h3>
        <FormContext.Provider
          value={{
            ...state,
            setValue,
            setError,
          }}
        >
          {children}
        </FormContext.Provider>
      </div>
    )
}

export default Form;