import React, {useContext, useState} from 'react';
import FormContext from '../FormContext';

const CvvInput = ({
        name,
        placeholder,
        type = 'text',
        activeCheck = () => {},
        cardType = '',
        validation = () => {},
        ...props}) => {
    const {setValue, setError, values, errors} = useContext(FormContext);
    const [fieldValue, setFieldValue] = useState(values[name]);
    return (
        <div className="field">
            <input 
                placeholder={placeholder}
                name={name}
                type={type}
                onChange={({target}) => {
                    const {value} = target;
                    setFieldValue(value);
                    setValue(name, value);
                }}
                onBlur={() => {
                    const error = validation(fieldValue, cardType);
                    setError(name, error);
                }}
                {...props}
            />
            {errors[name] && (
                <div className="error">{errors[name]}</div>
            )}
        </div>
    )
}

export default CvvInput;