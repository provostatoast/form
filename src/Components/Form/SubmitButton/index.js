import React, {useContext} from 'react';

import FormContext from '../FormContext';
const SubmitButton = ({
        name,
        placeholder,
        onClick = () => {},
        ...props}) => {
    const {errors, values} = useContext(FormContext);
    const errorCount = Object.values(errors).filter((err) => {
        return err;
    }).length;

    return (
        <div className="field">
            <input disabled={errorCount > 0} onClick={() => onClick(errors, values)} className="submitButton" type="submit" value="Submit" />
            {errorCount > 0 && (
                <div className="error">Please fix errors</div>
            )}
        </div>
    )
}

export default SubmitButton;