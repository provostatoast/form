import React, {useContext, useState} from 'react';
import FormContext from '../FormContext';

const Input = ({
        name,
        placeholder,
        type = 'text',
        activeCheck = () => {},
        setType = () => {},
        validation = () => {},
        ...props}) => {
    const {setValue, setError, values, errors} = useContext(FormContext);
    const [fieldValue, setFieldValue] = useState(values[name]);
    return (
        <div className="field">
            <input 
                placeholder={placeholder}
                name={name}
                type={type}
                onChange={({target}) => {
                    const {value} = target;
                    setFieldValue(value);
                    setValue(name, value);
                }}
                onBlur={() => {
                    const error = validation(fieldValue);
                    setError(name, error);
                }}
                {...props}
            />
            {errors[name] && (
                <div className="error">{errors[name]}</div>
            )}
        </div>
    )
}

export default Input;