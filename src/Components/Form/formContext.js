import {createContext} from 'react';


const FormContext = createContext({
    values: {},
    errors: {},
    type: '',
})

export default FormContext;