import React, {useContext, useState} from 'react';
import FormContext from '../FormContext';

const YearInput = ({
        name,
        placeholder,
        type = 'text',
        activeCheck = () => {},
        setType = () => {},
        validation = () => {},
        ...props}) => {
    const {setValue, setError, values, errors} = useContext(FormContext);
    const [fieldValue, setFieldValue] = useState(values[name]);
    const {expMo, expYr} = values;
    return (
        <div className="field">
            <input 
                placeholder={placeholder}
                name={name}
                type={type}
                onChange={({target}) => {
                    const {value} = target;
                    setFieldValue(value);
                    setValue(name, value);
                }}
                onBlur={() => {

                    const expDate = new Date(expYr, expMo - 1);
                    const now = new Date();
                    const monthFromNow = now.getMonth() === 11
                        ? new Date(now.getFullYear() + 1, 0, 1)
                        : new Date(now.getFullYear(), now.getMonth() + 1, 1);
                    const error = validation(fieldValue);
                    setError(name, error);
                    if (expDate < monthFromNow) {
                        setError(name, 'Please enter a valid future expiration date.')
                    }
                }}
                {...props}
            />
            {errors[name] && (
                <div className="error">{errors[name]}</div>
            )}
        </div>
    )
}

export default YearInput;