import { useState } from "react/cjs/react.development";
import "./App.css";
// import cards from './cards.png';

import Form from "./Components/Form";
import Input from "./Components/Form/Input";
import CardInput from "./Components/Form/CardInput";
import CvvInput from "./Components/Form/CvvInput";
import MonthInput from "./Components/Form/MonthField";
import YearInput from './Components/Form/YearField';
import SubmitButton from './Components/Form/SubmitButton';

import {nameValidation, cardValidation, activeCardCheck, cvvValidation, monthValidation, yearValidation} from "./validation"

const initialState = {
  name: "",
  cardNumber: "",
  cvv: "",
  expMo: "",
  expYr: "",
}

function App() {
  const [cardType, setCardType] = useState('');
  const handleSubmit = (e, v) => {
    const errorCount = Object.values(e).filter((err) => {
      return err;
    }).length;
    const valuesCount = Object.values(v).filter((value) => {
      return value;
    }).length;

    if (errorCount === 0 && valuesCount === Object.keys(v).length) {
      alert('Success! The form is valid, now the payload would be sent off to be processed and we\'d just wait for a response from the server.');
    }
  }

  return (
    <div className="App">
      <Form
        values={initialState}
      >
        <div>
          <Input
            placeholder="Name"
            name="name"
            type="text"
            className="field"
            validation={nameValidation}
          />
        </div>
        <div>
          <CardInput
            placeholder="Card Number"
            name="cardNumber"
            type="text"
            autoComplete="off"
            className="field"
            validation={cardValidation}
            activeCheck={activeCardCheck}
            cardType={cardType}
            setCardType={setCardType}
          />
        </div>
        <div>
          <CvvInput cardType={cardType} validation={cvvValidation} placeholder="CVV" name="cvv" type="text" className="field" />
        </div>
        <div className="expContainer">
          <div><MonthInput placeholder="Exp. Mo" validation={monthValidation} name="expMo" type="number" min="1" max="12" className="" /></div>
          <div><YearInput placeholder="Exp. Yr" validation={yearValidation} name="expYr" type="number" min="2021" max="" className="" /></div>
        </div>
        <div className="cardLogos">
          <img src="./cards.png" alt="American Express, Visa" />
        </div>
        <SubmitButton onClick={handleSubmit} />
      </Form>
    </div>
  );
}

export default App;
