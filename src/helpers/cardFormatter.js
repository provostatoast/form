const types = {
    amex: {
      pattern: 'xxxx xxxxxx xxxxx',
      max: 17,
    },
    visa: {
      pattern: 'xxxx xxxx xxxx xxxx',
      max: 19,
    },
}

const defaultType = {
    pattern: 'xxxxxxxxxxxxxxxx',
    max: 16,
}

const formatCardField = (value, type) => {
    const numsOnly = value.replace(/[^0-9]/g, "");
    const {pattern} = type ? types[type] : defaultType;
    const chars = numsOnly.split('');
    let count = 0;
    
    let formattedValue = '';
    for (let i = 0; i < pattern.length; i++) {
        const c = pattern[i];
        if (chars[count]) {
            if (/x/.test(c)) {
                formattedValue += chars[count];
                count++;
            } else {
                formattedValue += c;
            }
        }
    }
    return {formattedValue, numsOnly};
}

export {formatCardField};
