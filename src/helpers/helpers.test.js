import { formatCardField } from "./cardFormatter";

test('Properly formats Amex cards', () => {
    const {formattedValue} = formatCardField('377777777777777', 'amex');
    expect(formattedValue).toBe('3777 777777 77777');
})

test('Properly formats Visa cards', () => {
    const {formattedValue} = formatCardField('4222333344445555', 'visa');
    expect(formattedValue).toBe('4222 3333 4444 5555');
})

test('Has a standard formatting for non-approved card types', () => {
    const {formattedValue} = formatCardField('4222333344445555');
    expect(formattedValue).toBe('4222333344445555');
})