import { nameValidation, cardValidation, activeCardCheck, cvvValidation, monthValidation } from "./validation";

// Name Field Validation
test('Name field cannot be blank', () => {
    const noError = nameValidation('Gert Frobe');
    const error = nameValidation('');
    expect(noError).toBe(undefined);
    expect(error).toBe('Name field cannot be blank.')
});

test('Name field only uses letters and spaces', () => {
    const noError = nameValidation('Gert B Frobe');
    const error = nameValidation('Gert B. Frobe');
    expect(noError).toBe(undefined);
    expect(error).toBe('Only letters and spaces allowed.')
});

test('AMEX cards must have a length of 15', () => {
    // 10 chars
    const tooLittle = cardValidation('3711111111', 'amex');
    // 18 chars
    const tooMuch = cardValidation('371111111111111111', 'amex');
    // 15 chars
    const justRight = cardValidation('371111111111111', 'amex');
    expect(tooLittle).toBe('Amex cards must have 15 characters.');
    expect(tooMuch).toBe('Amex cards must have 15 characters.');
    expect(justRight).toBe(undefined);
})

test('Visa cards must have a length of 16', () => {
    // 10 chars
    const tooLittle = cardValidation('4371111111', 'visa');
    // 18 chars
    const tooMuch = cardValidation('4371111111111111111111111', 'visa');
    // 15 chars
    const justRight = cardValidation('4371111111111111', 'visa');
    expect(tooLittle).toBe('Visa cards must have 16 characters.');
    expect(tooMuch).toBe('Visa cards must have 16 characters.');
    expect(justRight).toBe(undefined);
})

test('Only Visa or AMEX supported', () => {
    const invalidCardNumberCheck = activeCardCheck('4232322323a');
    const onlyNumbersCheck = activeCardCheck('42323223236');
    const invalidCardType = activeCardCheck('84504824539');
    const validCardType = activeCardCheck('4504824539');
    const validCardType2 = activeCardCheck('3404824539');
    const validCardType3 = activeCardCheck('3704824539');

    expect(invalidCardNumberCheck.error).toBe('Only numbers allowed.');
    expect(onlyNumbersCheck.error).toBe('');
    expect(invalidCardType.error).toBe('Please use a Visa or Amex card.');
    expect(validCardType.error).toBe('');
    expect(validCardType2.error).toBe('');
    expect(validCardType3.error).toBe('');
})

test('Ensures proper value of the CVV', () => {
    const workingVisa = cvvValidation('333', 'visa');
    const workingAmex = cvvValidation('3333', 'amex');
    const failingVisa = cvvValidation('3333', 'visa');
    const failingAmex = cvvValidation('333', 'amex');
    const invalidCardNumber = cvvValidation('333', '');

    expect(workingVisa).toBe(undefined);
    expect(workingAmex).toBe(undefined);
    expect(failingVisa).toBe('Please enter the 3 digit security code on the back of the card.');
    expect(failingAmex).toBe('Please enter the 4 digit security code on the front of the card.');
    expect(invalidCardNumber).toBe('Please enter a valid CVV number.');
})

test('Ensures proper value of the month field', () => {
    const workingMonth = monthValidation('11');

    expect(workingMonth).toBe(undefined);
})